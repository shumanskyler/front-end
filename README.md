# Real Value front end

_This project is, and will continue to be a WIP. Use at your own risk._

[![Netlify Status](https://api.netlify.com/api/v1/badges/07f93f7c-80a3-4a9b-a49d-7b86d7fffa8f/deploy-status)](https://app.netlify.com/sites/skyler-real-value/deploys)

[![Real-Value](https://img.shields.io/endpoint?url=https://dashboard.cypress.io/badge/detailed/fecxei/sveltekit&style=for-the-badge&logo=cypress)](https://dashboard.cypress.io/projects/fecxei/runs)

## What is this?

Real value is an attempt to provide users with advice on investments based in sound statistical reasoning, machine learning models, and time tested investment strategies. This particular portion of the project provides a user interface for interacting with the Hasura/GraphQL/Postgres database. Although the database exists and can issue tokens to users for 10 days of unlimited API calls, this client allows for the regular user to make use of the project. To see the back end, which handles most of the Hasura database and authorization, see: [Link not yet inserted]

## How was it built?

This is perhaps the most difficult area for me to answer. Although it has undergone numerous iterations and changes throughout the past semester, I have settled on a main technology stack centered around Hasura as my GraphQL management service, running on a Postgres flavored database called TimescaleDB. All the data was collected from AlphaVantage, or generated using the back end python code. The front end utilizes SvelteKit and Svelte hosted on a Netlify service. Charting is done with SvelteKit's builtin "Pancake" library, which effectively renders a SVG server side to be served as a static chart, vastly improving performance. The styling was done by me using TailwindCSS. Testing for front end uses Cypress, and my roommate (That's a joke). For links to documentation on these specific technologies, see below.

_A note on technologies utilized_

I began this project using React Meteor app. My charting library was a mix of react-timeseries charts, charts-js, and Trading View's charting library. Styling was done with Semantic UI. The back end utilized Meteor's built in MongoDB instance with Meteor user management.

Although Meteor makes it tremendously easy to get started developing, several common development recipes are done differently or not at all to maintain early simplicity. This makes development long term unsustainable.

For example, Meteor's user accounts seems like a life saver. Simply create a schema in the MongoDB instance, publish it, and then you can create and destroy users in real time on the client. Unfortunately, the client can completely destroy any user, and access is unrestricted. To fix this issue, you can implement hooks that were predesigned to handle common tasks, like creating and destroying users on the back end. Meteor's opinion though is that users should have access to very little data, and they make it impressively challenging to change this standard.

Finally, the largest issue with Meteor was storing time series data. Until just recently, MongoDB had no native support for time series (v5.0 introduced a bucketed time series format, which is significantly better than previously, but still worse than the alternative below), and the only potential solution (besides not storing everything or massive amounts of space requirements) was a convuluted schema approach called time bucketing. It involves building larger buckets with known intervals of time that capture only a few key data points, and don't associate them with times. Instead, they are sorted in order with a known and consistent interval. This is fairly computationally expensive for both the client and the back end. On top of all this, even when implemented well, stocks carry at minimum 5 float data points per time interval, and several hundred intervals per day. This runs into Meteor's free storage limit after ingesting data for just 1 ticker symbol and two years of data. AlphaVantage provides data for thousands of symbols, and 20 years of data.

This leads to a unique array of problems that TimescaleDB was well positioned to solve in a much more efficient manner than Meteor's MongoDB instance. Although TimescaleDB is nowhere near as efficient as InfluxDB, it was written as an extension to Postgres, and not an entirely new database with a proprietary querying language. It's also significantly more efficient than MongoDB bucketing. TimescaleDB uses hypertables, which is an abstraction to a similar concept as time buckets. The distinction is that individual datasets are stored in hash maps that point to their relevant time buckets that have been compressed in an extremely efficient manner.

## How to get setup

1. Clone the repository
2. Run `npm install` within the repositories root directory
3. Edit any of the `.svelte`, javascript, or `test.svelte` files in `/src`
4. Run `npm run dev` to serve the application on `localhost:3000`.
5. Run `npm run build` to build a production version of the application.
6. Run `npm run test` to test all files in `/src` using testing library and jest.

## API Documentation

### Chart Component

The chart component fetches data from GraphQL first, and from AlphaVantage if necessary. It then creates a visualization of the stock price from the closing price of the stock.

In order to utilize the chart component import it using:

`import ChartContainer from '$components/chartContainer.svelte';`

Then simply pass a symbol as you would any regular svelte component:

`<ChartContainer symbol={'AAPL'} />`

### Search Component

The search component relies primarily on the AlphaVantage search endpoint, although I am transitiioning it towards using the GraphQL endpoint to search.

To utilize it, you don't need to do anything differently than a normal svelte component:
```
<script>
import Search from '/stocks/search.svelte'
</script>
<Search />
```

### Dashboard Page

The dashboard page is missing quite a few features, and has some planned implementation in the future.

### Pancake Charting

Pancake's charting library documentation is abysmal. Mainly because Rich Harris is probably too busy to fix the documentation himself, and most of the components aren't incredibly complex to understand.

The first key to Pancake is that every Chart requires a `Pancake.Chart` element.
This element takes 4 variables: x1, x2, y1, y2. These variables define the bounds (think canvas size) of your chart.

The second element is totally optional, but I strongly reccomend it for any scatter or line charts: `Pancake.Grid`. This creates a background grid that matches up with the passed variable `count` for `n` axis, the `n` variable in a formula.

The third element is necessary for actually displaying data: `Pancake.Svg`. Pancake Svg is just a container for the actual stuff that makes up the chart. It's crucial if your goal is to display anything (which it should be).

The next component is `Pancake.`*DataFormat* where *DataFormat* could be SvgScatterplot, SvgLine, Quadtree, or any other relevant type as seen in the pancake examples. 

These components take a data variable, which is an array containing objects with your data points. The most important part of this is to pass the appropriate callback functions to these components. FOr this example, I will use a data variable structured like:
```
data = [
    {time: timestamp1, value: 0},
    {time: timestamp2, value: 1}, 
    {time: timestamp3, value: 3},
]
```
```
<Pancake.SvgScatterplot {data} x={(d) => d.time} y={(d) => d.value} let:d>
    <path {d}>
</Pancake.SvgScatterplot>
```
As you can see, the scatter plot maps data to an x and y value for the variable d, then passes it to the path nested within. This allows the path to draw a single point on the chart (or line, or bar, whatever is relevant fo r that particular chart type).

Pancake should also be used with a git submodule to avoid conflicts with the module type of svelte kit when testing (confusing why this is an issue if it was built for svelte kit).
Although not necessary with the current testing setup because of Cypress, it makes testing with Mocha, Testing Library, and Jest possible, and is strongly encouraged. I have not switched the current version to this.


This was my first time using git submodules, and it has quirky behavior that needs to be explained. After first cloning the main repository, you should clone the submodule:
```
git submodule init
git submodule update
```
Alternatively, you can use
```
git clone --recurse-submodules
```
when cloning the first time to avoid having to do this.
The submodule exists as its own independedent git repository, and will not reflect any changes on the remote unless synced manually.

### Authentication and Login

Authentication is done with Auth0-Spa-JS. There documentation is fairly explanaotry, but I did do some small things differently. Since the API requires tokenized authentication for *some* queries, I also store the token in localStorage for faster access. I drastically increased the token lifespan on Auth0 from 1 day to 100 days. 

If you'd like to get any data from the GraphQL endpoint that is role protected, you can likely grab the token using `F12` developer tools and looking at the network resources when you are logged in.

Ideally, the auth0 would be logged in with a redirect URL that would request the user's favorite stocks when the user is logged in. This could all be stored in localStorage to save on access time later.

### GraphQL queries

You can query the GraphQL endpoint in 2 ways:

1. Using urql (preferred)
```
<script>
    import { operationStore, query } from '@urql/svelte';

    const dataName = operationStore(`
        query {
            some_table(rules here) {
               desired_outputs 
            }
        }
    `);

    query(dataName);
    let result = await dataName.data.some_table;
</script>
```
2. Using built in fetch
```
<script>
    fetch('https://hasura.skylershuman.com/v1/graphql', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': jwt_token_here
        },
        body: JSON.stringify({query: "{ some_table(rules here) { desired_outputs } }"})
})
  .then(r => r.json())
  .then(data => console.log('data returned:', data));
</script>
```
I've had rather poor results with fetch, as sometimes, authentication does not work like it should.

### Environment variables

In addition to all the above, Vite provides a neat environment variable management service from `import.meta.env.VITE_` that allows you to hide or show variables as you wish. Currently, I don't have anything protected like I should, but I plan to add it in the future. To use VITE's environment variables, simpley create a file titled `.env.[mode]` where mode is the mode the application will be running in when that set of variables applies (`production` or `development` or `staging`).

Any variables not prefixed with `VITE_` are invisible to all but private components and endpoints with the `_` prefix on them ins svelte kit.

In addition to adding Vite environment variables, you may need to add Cypress environment variables to a file called `cypress.env.json`.

You can access them by using `Cypress.env.get('variable_name')`.

### Vite aliases

Some of the folders in this project are aliased through vite. This is done in `svelte.config.js` by adding the below snippet to the vite configuration:

```
resolve: {
    alias: {
        $components: path.resolve('./src/components'),
        $auth: path.resolve('./src/auth'),
        $stores: path.resolve('./src/stores'),
        $actions: path.resolve('./src/actions'),
        $env: path.resolve('./src/env')
    }
}
```

This allows you to do things like import a component from `./src/components` using the syntax below:
```
import Component from '$components/Component';
```
*Note: Normal import statements will still work.*

### Testing
Any tests should be added to `cypress/integration/` under the appropriate location in the file naming convention `testname.spec.js`.

For further documentation see the Cypress testing docs.

### Netlify

Finally, a `netlify.toml` file is included. This allows you to include serverless functions. The current directory serverless functions are added to is `src/actions` although that can easily be changed by modifying the functions variable in the .toml file.

For this particular project, a serverless function I plan to add would be to automatically update the stock market data anytime one of the files is found to be stale. I will need to look more into a good way of doing this.

### See also
