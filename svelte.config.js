import preprocess from 'svelte-preprocess';
import adapter from '@sveltejs/adapter-netlify';
import path from 'path';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	kit: {
		// hydrate the <div id="svelte"> element in src/app.html
		adapter: adapter(),
		target: '#svelte',
		vite: {
			resolve: {
				alias: {
					$components: path.resolve('./src/components'),
					$auth: path.resolve('./src/auth'),
					$stores: path.resolve('./src/stores'),
					$actions: path.resolve('./src/actions'),
					$env: path.resolve('./src/env')
				}
			},
			optimizeDeps: {
				exclude: ['@urql/svelte'],
			}
		}
	},

	preprocess: [
		preprocess({
			postcss: true
		})
	]
};

export default config;
