import writable from './localStore';
import { writable as tempStore } from 'svelte/store';

export const isAuthenticated = writable('authenticated', false);
export const user = writable('user', {});
export const popupOpen = tempStore(false);
export const error = tempStore();
