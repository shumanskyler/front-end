describe('Navigation', () => {
	beforeEach(() => {
		cy.viewport(375, 812);
		cy.visit('http://localhost:3000');
		cy.get('[id=open-mobile-menu]').click();
	});

	it('Has clickable content titled "Real Value"', () => {
		cy.contains('Real Value').click();
	});

	it('Has a stocks link', () => {
		cy.get('[id=mobile-menu]').contains('Stocks').click();

		cy.url().should('include', '/stocks');
	});

	it('Has a cryptocurrency link', () => {
		cy.get('[id=mobile-menu]').contains('Cryptocurrencies').click();

		cy.url().should('include', '/crypto');
	});

	it('Has a notifications badge that opens a notifications box', () => {
		cy.contains('notifications').click();

		cy.contains('Notifications');
	});

	it('Has a sign in button', () => {
		cy.contains('Sign In').click();
	});
});
